<?php
/* SET to display all warnings in development. Comment next two lines out for production mode*/
ini_set('display_errors','On');
error_reporting(E_ALL);

/* Set the path to the Application folder */
DEFINE("LIB",$_SERVER['DOCUMENT_ROOT']."/lib/");

/* SET VIEWS path */
DEFINE("VIEWS",LIB."views/");
// DEFINE("PARTIALS",VIEWS."/partials");

//Define paths to static files
define("CSSPATH", "/build/main.bundle.css");
define("JSPATH", "/build/main.bundle.js");

# Paths to actual files
// DEFINE("MODEL",LIB."/model.php");
DEFINE("APP",LIB."/application.php");

# Define a layout
DEFINE("LAYOUT","master");

DEFINE("CONTENT","content");

# This inserts our application code which handles the requests and other things
require APP;


/* Here is our Controller code i.e. API if you like.  */
/* The following are just examples of how you might set up a basic app with authentication */

get("/",function($app){

  $app->set_message("title","Home");
  $app->render(LAYOUT,"home");
});

get("/population",function($app){

  $app->set_message("title","Population");
  $app->render(CONTENT,"population");
});

get("/dummy",function($app){

  $app->set_message("title","Dummy");
  $app->render(CONTENT,"dummy");
});

get("/health",function($app){

  $app->set_message("title","Health");
  $app->render(CONTENT,"health");
});


get("/innerPage",function($app){

  $app->set_message("title","Inner Page");
  $app->render(CONTENT,"innerPage");
});



# The Delete call back is left for you to work out

// New. If it get this far then page not found
resolve();
