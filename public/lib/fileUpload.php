<?php

$strJson = "{\"success\":false}";
if($_POST['file']){
  $rawData =  $_POST['file'];
  $filteredData = explode(',', $rawData );
  $unencoded = base64_decode($filteredData[1]);
  // echo $unencoded;
  $randName = rand(0, 2147483647);
  $date = date_create();
  $timeStamp =  date_timestamp_get($date);
  $fileName = "../uploads/img.{$timeStamp}.{$randName}.jpg";

  try {
    $fp = fopen($fileName, 'wb');
    fwrite($fp, $unencoded);
    $strJson = "{\"success\":true, \"fileName\": \"$fileName\"}";
  } catch(Exception $e) {
    echo $e->getMessage();
  } finally {
    fclose($fp);
  }

}
header("Content-Type: application/json; charset=utf-8");

echo $strJson;

?>
