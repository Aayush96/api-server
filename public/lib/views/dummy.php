<!-- <section class="event-card__outer-wrap">
  <div class="container">
    <h2 class="event-card__grid-title">Event Grid Name</h2>
    <div class="event-card__grid">

      <article class="event-card col-sm-6">
        <div class="event-card__text">
          <div class="event-card__header">
            <img class="event-card__img" src="https://unsplash.it/400/300/" alt="#">
            <h3 class="event-card__combined-heading combined-heading">
              <strong class="event-card__title combined-heading__title">Dataset name</strong>
            </h3>
          </div>

          <div class="rte">
            <p>a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes</p>
            <p>a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes</p>
            <p>a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes</p>
          </div>
          <div class="event-card__btn-wrap">
            <a href="#" class="event-card__btn btn btn--outline">Sign Up</a>
          </div>
        </div>
      </article>
      <article class="event-card col-sm-6">
        <div class="event-card__text">
          <div class="event-card__header">
            <img class="event-card__img" src="https://unsplash.it/200/300/" alt="#">
            <h3 class="event-card__combined-heading combined-heading">
              <strong class="event-card__title combined-heading__title">Event Name</strong>
              <span class="combined-heading__tagline">Prize: $500</span>
            </h3>
          </div>

          <div class="rte">
            <p>a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes</p>
            <p>a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes</p>
          </div>
          <div class="event-card__btn-wrap">
            <a href="#" class="event-card__btn btn btn--outline">Sign Up</a>
          </div>
        </div>
      </article>
      <article class="event-card col-sm-6">
        <div class="event-card__text">
          <div class="event-card__header">
            <img class="event-card__img" src="https://unsplash.it/200/300/" alt="#">
            <h3 class="event-card__combined-heading combined-heading">
              <strong class="event-card__title combined-heading__title">Event Name</strong>
              <span class="combined-heading__tagline">Prize: $500</span>
            </h3>
          </div>

          <div class="rte">
            <p>a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes</p>
            <p>a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes</p>
            <p>a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes</p>
          </div>
          <div class="event-card__btn-wrap">
            <a href="#" class="event-card__btn btn btn--outline">Sign Up</a>
          </div>
        </div>
      </article>
      <article class="event-card col-sm-6">
        <div class="event-card__text">
          <div class="event-card__header">
            <img class="event-card__img" src="https://unsplash.it/200/300/" alt="#">
            <h3 class="event-card__combined-heading combined-heading">
              <strong class="event-card__title combined-heading__title">Event Name</strong>
              <span class="combined-heading__tagline">Prize: $500</span>
            </h3>
          </div>

          <div class="rte">
            <p>a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes</p>
            <p>a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes</p>
          </div>
          <div class="event-card__btn-wrap">
            <a href="#" class="event-card__btn btn btn--outline">Sign Up</a>
          </div>
        </div>
      </article>
    </div>
  </div>
</section> -->
<section class="container mt-3 ">
<div class="row">
          <div class="col-4">
              <div class="card-box">
                  <div class="card-title">
                      <h2>Lorem ipsum dolor si.</h2>
                      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda, </p>
                  </div>
                  <div class="card-link">
                      <a href="" class="c-link">Learn More
                          <i class="fa fa-angle-right"></i>
                      </a>
                  </div>
              </div>
          </div>
          <div class="col-4">
              <div class="card-box">
                  <div class="card-title">
                      <h2>Lorem ipsum dolor si.</h2>
                      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda, </p>
                  </div>
                  <div class="card-link">
                      <a href="" class="c-link">Learn More
                          <i class="fa fa-angle-right"></i>
                      </a>
                  </div>
              </div>
          </div>
          <div class="col-4">
              <div class="card-box">
                  <div class="card-title">
                      <h2>Lorem ipsum dolor si.</h2>
                      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda, </p>
                  </div>
                  <div class="card-link">
                      <a href="" class="c-link">Learn More
                          <i class="fa fa-angle-right"></i>
                      </a>
                  </div>
              </div>
          </div>

      </div>

</section>
