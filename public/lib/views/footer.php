</main>
<footer class="footer">
  <div class="container">

    <div class="footer__credits">
      <h2 class="footer__credits-title">Developer Details</h2>
      <div class="footer__credits-wrap">
        <div class="footer__credits-item col-sm-6 col-md-4 col-lg-3">
          <h3 class="footer__credits-name">&#9786; Aayush Sapkota</h3>
          <p>&#x2709; 96.estd@gmail.com</p>
        </div>

        <div class="footer__credits-item col-sm-6 col-md-4 col-lg-3">
          <h3 class="footer__credits-name">&#9786; Aasish Adhikari</h3>
          <p>&#x2709; gemini.aasis@gmail.com</p>
        </div>
      </div>
    </div>
    <hr>

    <div class="footer__inner-wrap footer__details">
      <div class="footer__item col-sm-6">
        <h3 class="footer__item-title">Site map</h3>
        <nav class="footer__links rte">
          <ul class="footer__links-list">
            <?php if(isset($is_authenticated) && $is_authenticated === true){ ?>
              <li><a href="/afterloginhome">Home</a></li>
              <li><a href="/user">Account</a></li>
              <li><a href="/classes">Classes</a></li>
              <li><a href="/records">Records</a></li>
              <form action="/signout" method="get">
                <button class="btn" type="submit">Sign out</button>
              </form>
            <?php } else {?>
              <li><a href="/">Home</a></li>
              <li><a href="/signup">Sign Up</a></li>
            <?php } ?>
          </ul>
        </nav>

        <p class="footer-company-name">Data API System &copy; 2018, Nobody</p>
      </div>
      </div>
    </footer>
    <script type="text/javascript" src="<?php echo JSPATH; ?>"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
  </html>
