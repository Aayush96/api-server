<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Open Data - <?php echo $title; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="<?php echo CSSPATH;?>" />
  <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>
<body>

  <div id="mobile-bar">
    <a href="/">
      <h3 class="mobile-nav__title">Facial Recog</h3>
    </a>
  </div>
  <input type="checkbox" class="check" id="checked">
  <label class="menu-btn" for="checked">
    <span class="bar top"></span>
    <span class="bar middle"></span>
    <span class="bar bottom"></span>
    <span class="menu-btn__text"></span>
  </label>
  <label class="close-menu" for="checked"></label>
  <nav class="drawer-menu">
    <ul>
      <?php if(isset($is_authenticated) && $is_authenticated === true){ ?>
        <li><a href="/afterloginhome">Home</a></li>
        <form action="/signout" method="get">
          <button class="btn" type="submit">Sign out</button>
        </form>
      <?php } else {?>
        <li><a href="/">Home</a></li>
      <?php } ?>
    </ul>
  </nav>

  <div id="desk-nav">

    <a href="/">
      <img class="desk-nav__logo" src="./logo.jpg" alt="Logo">
    </a>
    <nav class="desk-nav">
      <ul>
        <?php if(isset($is_authenticated) && $is_authenticated === true){ ?>
          <li><a href="/afterloginhome">Home</a></li>
          <form action="/signout"  method="get">
            <button class="btn" type="submit">Sign out</button>
          </form>
        <?php } else {?>
          <li><a href="/">Home</a></li>
        <?php } ?>
      </ul>
    </nav>
  </div>
