

<section class="summary-card__outer-warp">
  <div class="container">
    <h2 class="summary-card__grid-title text-center">Open Data Portal</h2>
    <!-- Search form -->
    <div class="h-50  mb-5">
<form class="form-inline md-form form-lg col-12 h-50 d-flex justify-content-center">
    <input class="w-75 form-control form-control-lg mr-3 w-75" type="text" placeholder="Search" aria-label="Search">
    <i class="fa fa-search" aria-hidden="true"></i>
</form>
</div>

    <div class="summary-card__grid justify-content-center text-center">
      <a href="/population" class="summary-card">
        <span class="icon-size icon-group"></span>
        <p>Population</p>
      </a>
      <a href="/population" class="summary-card"><span class="icon-size icon-pagelines"></span>
        <p>Environment</p>
      </a>
      <a href="/population" class="summary-card"><span class="icon-size icon-office"></span>
        <p>Infastructures</p>
      </a>
      <a href="/population" class="summary-card"><span class="icon-size icon-justice"></span>
        <p>Justice</p>
      </a>
      <a href="/population" class="summary-card">
        <span class="icon-size icon-heartbeat"></span>
        <p>Health</p>
      </a>
      <a href="/population" class="summary-card">
        <span class="icon-size icon-money-bag"></span>
        <p>Finance</p>
      </a>
    </div>
  </div>
</section>
