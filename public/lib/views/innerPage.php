<div class="container rte wrap">
  <h1>This is an example inner page</h1>
  <p>This will be a short description about the content of the site and the ways the users can access it. Explain more if you have a note or specific comments</p>


  <h3>Documentation For API if present</h3>

  <h3>Short subtitle for topic like ways to do get requests </h3>
  <p>Dummy content Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </p>
  <pre class="bg-light border-dark border-left">
    <code>
      var Baseurl = "http://dataportal.dummysetupapi.com.au/";

      var parametersAvailable = {
        //all the parameters that the CSV file has
      }
    </code>
  </pre>


  <ul>
    <li><a href="#">List of other instructions</a></li>
  </ul>
  <h4>Resources</h4>
  <p>Other resources short description</p>

  <h4>List of Downloadable sources</h4>

  <ul>
    <li><a href="#"></a>Example Source</li>
    <li><a href="#"></a>Example Source</li>
    <li><a href="#"></a>Example Source</li>
    <li><a href="#"></a>Example Source</li>
    <li><a href="#"></a>Example Source</li>
    <li><a href="#"></a>Example Source</li>
  </ul>

</div>
