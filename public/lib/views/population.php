<section class="">
  <p></p>
  <p></p>
  <div class="well container">
    <form class="form-horizontal row">
      <div class="form-group col-6">
        <label for="location1" class="control-label">Category</label>
        <select class="form-control" name="" id="location1">
          <option value="">Any</option>
          <option value="">population</option>
          <option value="">Health</option>
        </select>
      </div>
      <div class="form-group col-6">
        <label for="type1" class="control-label">Type</label>
        <select class="form-control" name="" id="type1">
          <option value="">Date</option>
          <option value="">2011</option>
          <option value="">2012</option>
        </select>
      </div>
      <div class="form-group col-8">
        <label for="pricefrom" class="control-label">Location</label>
        <div class="input-group">
          <input type="text" class="form-control" id="pricefrom" aria-describedby="basic-addon1">
        </div>
      </div>
      <p class="text-center col-3"><a href="#" class="btn btn-danger glyphicon glyphicon-search" role="button">Filter</a></p>
    </form>
  </div>
  <div class="container mt-3 ">
    <div class="row">
      <div class="col-4">
        <div class="card-box">
          <div class="card-title">
            <h2>Department of Housing and Community Development</h2>
            <p>BushTel is a central point for information about the remote communities of the Northern Territory, their people and cultural and historical influences. BushTel aims to enhance planning and informed decision making.</p>
          </div>
          <div class="card-link">
            <a href="https://bushtel.nt.gov.au/" class="c-link">Go to website
              <i class="fa fa-angle-right"></i>
            </a>
          </div>
        </div>
      </div>
      <div class="col-4">
        <div class="card-box">
          <div class="card-title">
            <h2>The National Map - Department of Prime Minister and Cabinet</h2>
            <p>National Map is a website for map-based access to spatial data from Australian government agencies. It is an initiative of the Department of Communications and the Arts now currently managed by the Department of the Prime Minister and Cabinet and the software has been developed by Data61 working closely with the Department of Communications and the Arts, Geoscience Australia and other government agencies.</p>
          </div>
          <div class="card-link">
            <a href="http://nationalmap.gov.au/" class="c-link">go to website
              <i class="fa fa-angle-right"></i>
            </a>
          </div>
        </div>
      </div>
      <div class="col-4">
        <div class="card-box">
          <div class="card-title">
            <h2>Australian Bureau of Statistics - API Site</h2>
            <p>Statistical, high level snapshot and overview of the Northern Territory, covering themes such as population and people, economy and industry, income, education and employment, health and disability, family and community and land and environment.</p>
            <p><b>Note - this page can be a little slow to load in comparison to other websites</b></p>
            <p>Review Past & Future Releases for the latest release</p>
          </div>
          <div class="card-link">
            <a href="/innerPage" class="c-link">Learn More
              <i class="fa fa-angle-right"></i>
            </a>
          </div>
        </div>
      </div>
      <div class="col-4">
        <div class="card-box">
          <div class="card-title">
            <h2>Australian Bureau of Statistics - Data by region</h2>
            <p>Data by Region provides you ease of access to statistics from the ABS and other sources on a particular geographical region. Use the tools to browse, search or explore by map to find statistics about different regions in Australia.</p>
          </div>
          <div class="card-link">
            <a href="/innerPage" class="c-link">Learn More
              <i class="fa fa-angle-right"></i>
            </a>
          </div>
        </div>
      </div>

      <div class="col-4">
        <div class="card-box">
          <div class="card-title">
            <h2>Australian Bureau of Statistics - Data by region</h2>
            <p>Data by Region provides you ease of access to statistics from the ABS and other sources on a particular geographical region. Use the tools to browse, search or explore by map to find statistics about different regions in Australia.</p>
          </div>
          <div class="card-link">
            <a href="/innerPage" class="c-link">Learn More
              <i class="fa fa-angle-right"></i>
            </a>
          </div>
        </div>
      </div>

      <div class="col-4">
        <div class="card-box">
          <div class="card-title">
            <h2>Australian Bureau of Statistics - Data by region</h2>
            <p>Data by Region provides you ease of access to statistics from the ABS and other sources on a particular geographical region. Use the tools to browse, search or explore by map to find statistics about different regions in Australia.</p>
          </div>
          <div class="card-link">
            <a href="/innerPage" class="c-link">Learn More
              <i class="fa fa-angle-right"></i>
            </a>
          </div>
        </div>
      </div>

      <div class="col-4">
        <div class="card-box">
          <div class="card-title">
            <h2>Australian Bureau of Statistics - Data by region</h2>
            <p>Data by Region provides you ease of access to statistics from the ABS and other sources on a particular geographical region. Use the tools to browse, search or explore by map to find statistics about different regions in Australia.</p>
          </div>
          <div class="card-link">
            <a href="/innerPage" class="c-link">Learn More
              <i class="fa fa-angle-right"></i>
            </a>
          </div>
        </div>
      </div>

      <div class="col-4">
        <div class="card-box">
          <div class="card-title">
            <h2>Australian Bureau of Statistics - Data by region</h2>
            <p>Data by Region provides you ease of access to statistics from the ABS and other sources on a particular geographical region. Use the tools to browse, search or explore by map to find statistics about different regions in Australia.</p>
          </div>
          <div class="card-link">
            <a href="/innerPage" class="c-link">Learn More
              <i class="fa fa-angle-right"></i>
            </a>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
