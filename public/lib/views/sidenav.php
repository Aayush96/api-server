<main class="d-flex flex-wrap justify-content-between flex-nowrap">
  <nav class="navbar side-nav navbar-light bg-light px-0 flex-column">
  <div class="w-100">
    <a class="navbar-brand" href="#">Category</a>
  <button id="collapse-btn" class="navbar-toggler border-0" type="button">
    <i class="fa fa-chevron-down pull-right"></i>
  </button>
  <div>
        <!-- <div class="sidebar-header">
            <h3>Categories</h3>
        </div> -->
        <ul class="navbar-nav mr-auto text-center">

            <li class="navbar-item p-lg-3">
                <a href="/population">
                  <span class="icon-group"></span>
                  <p>Population</p></a>
            </li>
            <li class="navbar-item p-3">
                <a href="/dummy">
                  <span class="icon-pagelines"></span>
                    <p>Environment</p></a>
            </li>
            <li class="navbar-item p-3">
                <a href="/dummy">
                  <span class="icon-office"></span>
                    <p>Infastructures</p></a>
            </li>
            <li class="navbar-item p-3">
                <a href="/dummy">
                  <span class="icon-justice"></span>
                  <p>Justice</p>
                </a>
            </li>
            <li class="navbar-item p-3">
                <a href="/dummy">
                  <span class="icon-heartbeat"></span>
                  <p>Health</p>
                </a>
            </li>

            <li class="navbar-item p-3">
                <a href="/dummy">
                  <span class="icon-money-bag"></span>
                  <p>Finance</p>
                </a>
              </li>
        </ul>
      </div>
    </div>
    </nav>
